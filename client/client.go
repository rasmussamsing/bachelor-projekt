// The code that the client runs. First requests public key from the trusted third party,
// then it generates a vote, encrypts it under the public key, and then sends it off to the tallying server.

package main

import (
	"bachelor-projekt/elgamal"
	"bachelor-projekt/helpers"
	"bachelor-projekt/network"
	"bachelor-projekt/packet"
	"bachelor-projekt/rsa"
	zkproofs "bachelor-projekt/zk_proofs"
	"encoding/json"
	"math/big"
	"os"
)

/*
	Retrieve pk and sk, and send two votes to the tallying server. First vote is accepted, and second vote is rejected,
	as each client may only vote once.
*/
func main() {
	pk, sk := helpers.GetKeyPair()
	user := os.Args[2]

	sendVote(user, pk, sk)
	sendVote(user, pk, sk)
}

/*
	Sends a vote=1 to the tallying server.

	userID: Unique ID given to each user
	pk_signing: Public RSA key of the user
	sk_signing: Secret RSA key of the user
*/
func sendVote(userID string, pk_signing rsa.PublicKey, sk_signing rsa.SecretKey) {
	// Request elgamal PK from trusted dealer.
	me, _ := network.FindEntityById(userID)
	me.Sk = sk_signing
	dealer, _ := network.FindEntityById("trusted_dealer_0")

	conn := network.Connect(me, dealer)
	conn.SendPacket([]byte{}, packet.PkRequest)

	// Retrieve elgamal PK from trusted dealer.

	p := conn.ReceivePacket()
	var pk elgamal.Public_key

	if p.Type == packet.TDPk {
		err := json.Unmarshal(p.Data, &pk)
		if err != nil {
			panic(err)
		}
	} else {
		panic("Wrong packet type from dealer.")
	}
	conn.Close()

	// Generate vote and send it to the tallying server

	tally, _ := network.FindEntityById("tally_0")
	conn = network.Connect(me, tally)

	defer conn.Close()
	generateAndSendVote(conn, pk)
}

/*
	Sends a vote=1 over the connection

	conn: Connection over which the vote is sent.
	pk: ElGamal pk to use for encryption.
*/
func generateAndSendVote(conn network.AuthenticatedConnection, pk elgamal.Public_key) {
	// Generate vote
	v := int64(1)
	vote := big.NewInt(0).Exp(&pk.G, big.NewInt(v), &pk.P)
	encryptedVote1, encryptedVote2, seed := elgamal.Encrypt(vote, pk)

	// Encrypt vote
	cipher := elgamal.Ciphertext{C1: encryptedVote1, C2: encryptedVote2}

	// Construct zk-proof that vote=0 OR vote=1
	proof := constructProof(v, cipher, seed, pk, conn.Me.Id)
	if !zkproofs.VerifyORProof(proof, cipher, pk, conn.Me.Id) {
		panic("Failed my own proof :(")
	}

	// Send everything to the tally server
	cipherProof := zkproofs.CipherAndProof{Cipher: cipher, Proof: proof}
	conn.SendPacket(&cipherProof, packet.Vote)
}

/*
	Constructs OR proof for the given vote.
	v: the vote that the user wishes to vote for
	cipher: The ciphertext to be sent to the tally server
	seed: When generating cipher.C1, we set it to g^x, where x is a randomly selected element of our group. Seed is that value.
	pk: Elgamal pk to use for the proof
*/
func constructProof(v int64, cipher elgamal.Ciphertext, seed *big.Int, pk elgamal.Public_key, userID string) zkproofs.ORProof {
	// Generate 'real' and 'fake' proof
	fakeProof := zkproofs.ConstructFakeProof(cipher, seed, pk, int(v), userID)
	realProof := zkproofs.ConstructRealProofFromFakeProof(cipher, seed, pk, fakeProof, int(v))

	if v == 1 { // this if-statement ensures that the proof for 0 is in P0 and proof for 1 is in P1
		return zkproofs.ORProof{P0: fakeProof, P1: realProof, UserID: userID}
	} else {
		return zkproofs.ORProof{P0: realProof, P1: fakeProof, UserID: userID}
	}
}
