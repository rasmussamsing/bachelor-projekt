// Contains general helpers that are used all over our project

package helpers

import (
	"bachelor-projekt/logging"
	"bachelor-projekt/network"
	"bachelor-projekt/rsa"
	"bufio"
	"crypto/rand"
	"fmt"
	"log"
	"math/big"
	"os"
	"strconv"
)

// Helper for checking whether an error occurred
func Error_check(e error) {
	if e != nil {
		panic(e)
	}
}

type Result struct {
	Counter   *big.Int
	Decryptor string
	Signature string
	PacketID  string
	Type      string
}

/*
	Returns the user's/server's rsa pk and sk. Assumes that the sk and userID is passed through the cli
*/
func GetKeyPair() (rsa.PublicKey, rsa.SecretKey) {
	// Read sk and entity_id from os.args
	d, ok := big.NewInt(0).SetString(os.Args[1], 10)
	entity_id := os.Args[2]

	if !ok {
		panic("bitch")
	}

	// Find entity and return pk, sk
	entity, _ := network.FindEntityById(entity_id)
	_, sk := rsa.ConstructKeys(entity.Pk.N, d)
	return entity.Pk, sk
}

/*
	Takes an rsa PK as input, and looks through the environment in search of the key owner. If found, returns the owner.
*/
func FindKeyOwner(pk rsa.PublicKey) string {
	n := pk.N
	var keyOwner string

	if n == nil {
		fmt.Println("Key has not been set!")
		return ""
	}
	v, ok := os.LookupEnv("KEY_COUNT")
	if !ok {
		fmt.Println("Could not find KEY_COUNT in env.")
		return ""
	}
	key_count, err := strconv.Atoi(v)

	if err != nil {
		fmt.Println("KEY_COUNT value not valid")
		return ""
	}
	// Look through all users, and check if one has the given pk
	for userID := 0; userID < key_count; userID++ {
		keyname := "pk_" + fmt.Sprint(userID)
		if keyIsCorrect(keyname, n) {
			keyOwner = keyname
		}
	}

	// Look through the tally, and check if one has the given pk
	tallyKeyName := "pk_tally_0"
	if keyIsCorrect(tallyKeyName, n) {
		keyOwner = tallyKeyName
	}

	// Look through the decryptors, and check if one has the given pk
	for decryptID := 0; decryptID < 5; decryptID++ {
		keyname := "pk_decryptor_" + fmt.Sprint(decryptID)
		if keyIsCorrect(keyname, n) {
			keyOwner = keyname
		}
	}

	// Check if the key corresponds to the trusted dealer
	trustedDealerKeyName := "pk_trusted_dealer_0"
	if keyIsCorrect(trustedDealerKeyName, n) {
		keyOwner = trustedDealerKeyName
	}

	return keyOwner
}

/*
	Returns TRUE if the key with name 'keyname' has 'n' as assigned value.
	Otherwise returns FALSE
*/
func keyIsCorrect(keyname string, n *big.Int) bool {
	val, ok := os.LookupEnv(keyname)
	if !ok {
		return false
	}
	n_from_env, ok := big.NewInt(0).SetString(val, 10)
	if !ok {
		return false
	}
	if n_from_env.Cmp(n) == 0 {
		return true
	}
	return false
}

/*
	Check whether the owner of the input PK has the given prefix.
*/

func KeyOwnerHasPrefix(pk rsa.PublicKey, prefix string) bool {
	keyOwner := FindKeyOwner(pk)
	if len(keyOwner) >= len(prefix) && keyOwner[0:len(prefix)] == prefix {
		return true
	}
	return false
}

/*
	Generates a random number with the given size
*/
func RandomNum(size int) *big.Int {
	var data = make([]byte, size)
	_, err := rand.Read(data)
	if err != nil {
		panic("Paniced while generating number")
	}
	num := big.NewInt(0).SetBytes(data)
	return num
}

/*
	Reads the env variable with name 'varname', and interprets it as an int
*/
func ReadInt(varname string, def int) int {
	valS, ok := os.LookupEnv(varname)
	if !ok {
		logging.Warning("Helpers", "Failed to read "+varname+" from env. Defaulting to "+fmt.Sprint(def))
		return def
	}
	val, err := strconv.Atoi(valS)
	if err != nil {
		logging.Warning("Helpers", "Failed to interpret "+varname+" as an int. Defaulting to "+fmt.Sprint(def))
		return def
	}
	return val
}

/*
	Reads the env variable with name 'varname', and interprets it as a string
*/
func ReadString(varname string, def string) string {
	valS, ok := os.LookupEnv(varname)
	if !ok {
		logging.Warning("Helpers", "Failed to read "+varname+" from env. Defaulting to "+def)
		return def
	}
	return valS
}

/*
	Computes the modular inverse of a wrt n.
*/
func ModularInverse(a *big.Int, n *big.Int) *big.Int {
	t := big.NewInt(0)
	newt := big.NewInt(1)
	r := n
	newr := a

	for newr.Cmp(big.NewInt(0)) != 0 {
		quotient := big.NewInt(0).Div(r, newr)
		t, newt = newt, big.NewInt(0).Sub(t, big.NewInt(0).Mul(quotient, newt))
		r, newr = newr, big.NewInt(0).Sub(r, big.NewInt(0).Mul(quotient, newr))
	}
	if r.Cmp(big.NewInt(1)) < 0 {
		panic("Not invertible")
	}
	if t.Cmp(big.NewInt(0)) < 0 {
		t.Add(t, n)
	}
	return t
}

func EnsurePrimesInEnv() (*big.Int, *big.Int) {
	p_res, found_p := os.LookupEnv("P_PRIME")
	q_res, found_q := os.LookupEnv("Q_PRIME")
	keySize := ReadInt("ELGAMAL_KEYSIZE", 4096)

	q := big.NewInt(0)
	p := big.NewInt(0)
	if found_p {
		p.SetString(p_res, 10)
	}
	if found_q {
		q.SetString(q_res, 10)
	}

	r := rand.Reader
	found_germain := false

	// Loop that continues until Germain prime has been found
	i := 0
	for !found_germain {
		if i != 0 {
			fmt.Println("Attempting iteration.. " + fmt.Sprint(i))
		}
		p = big.NewInt(0).Add(p.Mul(big.NewInt(2), q), big.NewInt(1))
		if p.ProbablyPrime(20) {
			found_germain = true
		} else {
			q, _ = rand.Prime(r, keySize)
		}
		i += 1
	}
	if !found_p || !found_q {
		os.Setenv("P_PRIME", p.String())
		os.Setenv("Q_PRIME", q.String())
		writePQToFile(p, q)
	}

	return p, q
}

func writePQToFile(p *big.Int, q *big.Int) {

	proj_folder, ok := os.LookupEnv("proj_folder")
	if !ok {
		panic("Could not find proj_folder env variable")
	}

	file, err := os.Open(proj_folder + "/configuration/bach.env")
	if err != nil {
		fmt.Println("Could not load bach.env file")
	}
	reader := bufio.NewReader(file)
	scanner := bufio.NewScanner(reader)
	lines := []string{}
	for scanner.Scan() {
		line := scanner.Text()
		lines = append(lines, line)
	}
	lines = append(lines, "P_PRIME="+p.String())
	lines = append(lines, "Q_PRIME="+q.String())
	file.Close()
	file, err = os.Create(proj_folder + "/configuration/bach.env")
	writer := bufio.NewWriter(file)
	for _, line := range lines {
		_, err := writer.Write([]byte(line + "\n"))
		if err != nil {
			log.Fatalf("Got error while writing to a file. Err: %s", err.Error())
		}
	}
	writer.Flush()
	file.Close()
}

func EnvVarToPrefix(envVar string) string {
	switch envVar {
	case "TRUSTED_DEALER_ADDRESS":
		return "pk_trusted_dealer"
	case "TALLY_ADDRESS":
		return "pk_tally"
	case "DECRYPT_ADDRESS":
		return "pk_decryptor"
	default:
		panic("Address not valid")
	}
}
