package helpers

import (
	"fmt"
	"testing"
)

func TestRandomNum(t *testing.T) {
	size := 1024
	nrOfTests := 500
	prevNumber := RandomNum(size)
	for i := 0; i > nrOfTests; i++ {
		number := RandomNum(size)
		if number != prevNumber {
			t.Errorf("The random number doesn't have the correct length" + fmt.Sprint(len(number.String())))
		}
		prevNumber = number
	}

}
