/*
	Startup script used to generate secret_keys.env and entities.json
*/

package main

import (
	"bachelor-projekt/helpers"
	"bachelor-projekt/logging"
	"bachelor-projekt/network"
	"bachelor-projekt/rsa"
	"bufio"
	"encoding/json"
	"fmt"
	"log"
	"math/big"
	"os"
	"strconv"
	"strings"
)

/*
	Deletes all existing keys, and regenerates them.
*/
func main() {
	// first ensure that we have Q and P primes
	helpers.EnsurePrimesInEnv()
	errMsg, keyFilesPresent := checkKeyFilesPresent()
	if keyFilesPresent && errMsg != "" {
		logging.Error("Key generator", "Failed interpreting keys with error message: '"+errMsg+"'")
		logging.Error("Hint", "Delete both entities.json and secret_keys.env, or reduce the amount of servers/voters in the system")
		os.Exit(1)
	}
	if keyFilesPresent && errMsg == "" {
		logging.Info("Key generator", "Key files are present and valid.")
		os.Exit(0)
	}
	logging.Info("Key generator", "No key files located. Generating all keys and entities and writing to file")
	clearKeyFiles()
	// Lookup the voter count in env.
	voterCountS, ok := os.LookupEnv("VOTER_COUNT")
	if !ok {
		fmt.Println("Could not find VOTER_COUNT. Using 5")
		voterCountS = "5"
	}
	voterCount, err := strconv.Atoi(voterCountS)
	if err != nil {
		voterCount = 5
	}

	// Construct entities corresponding to how many we need
	voters, sk_voters := ConstructEntities(voterCount, network.Voter, "voter")
	tally, sk_tally := ConstructEntities(1, network.Tally, "tally")
	decryptors, sk_decryptors := ConstructEntities(5, network.Decryptor, "decryptor")
	trusted_dealer, sk_trusted_dealer := ConstructEntities(1, network.TrustedDealer, "trusted_dealer")
	bulletin_board, sk_bulletin_board := ConstructEntities(1, network.BulletinBoard, "bulletin_board")

	// Create array of all entities, and of all secret keys
	entities := append(voters, append(tally, append(decryptors, append(trusted_dealer, bulletin_board...)...)...)...)
	secretKeys := append(sk_voters, append(sk_tally, append(sk_decryptors, append(sk_trusted_dealer, sk_bulletin_board...)...)...)...)

	// Write both arrays to their own files
	writeEntityFile(entities)
	writeSecretFile(secretKeys)
}

func getFilePaths() (string, string) {
	secretKeyPath := helpers.ReadString("proj_folder", "/go/src/bachelor-projekt") + "/configuration/secret_keys.env"
	entitiesPath := helpers.ReadString("proj_folder", "/go/src/bachelor-projekt") + "/configuration/entities.json"
	return secretKeyPath, entitiesPath
}

func checkKeyFilesPresent() (string, bool) {
	secretKeyPath, entitiesPath := getFilePaths()
	_, errSecret := os.Stat(secretKeyPath)
	_, errEntity := os.Stat(entitiesPath)
	if (errSecret != nil) != (errEntity != nil) {
		return "Error on looking up files.\nsecret_keys.env(" + (OnelineIf(errSecret == nil, "Found", "Not found")).(string) + ")\nentities.json(" + OnelineIf(errEntity == nil, "Found", "Not found").(string) + ")", true
	}
	if (errSecret != nil) && (errEntity != nil) {
		return "", false
	}

	// at this point, we know that the key files are both present
	// now we have to count the amount of keys required
	decryptCount := helpers.ReadInt("DECRYPT_COUNT", 1)
	tallyCount := 1
	voterCount := helpers.ReadInt("VOTER_COUNT", 1)

	_, msgV := checkSufficientEntities("voter_", voterCount, network.Voter)
	_, msgT := checkSufficientEntities("tally_", tallyCount, network.Tally)
	_, msgD := checkSufficientEntities("decryptor_", decryptCount, network.Decryptor)
	_, msgB := checkSufficientEntities("bulletin_board_", 1, network.BulletinBoard)
	_, msgTD := checkSufficientEntities("trusted_dealer_", 1, network.TrustedDealer)

	ok := true
	errMsg := ""
	for _, msg := range []string{msgV, msgT, msgD, msgB, msgTD} {
		if msg != "" {
			errMsg = OnelineIf(errMsg == "", msg, errMsg+" | "+msg).(string)
		}
	}
	return errMsg, ok
}

func checkSufficientEntities(prefix string, count int, entityType int) (bool, string) {
	secretKeysPath, _ := getFilePaths()
	skPrefix := "sk_" + prefix
	secretKeysFile, _ := os.Open(secretKeysPath)
	bitsS := helpers.ReadString("RSA_KEYSIZE", "1")
	bits, ok := big.NewInt(0).SetString(bitsS, 10)
	modulus := big.NewInt(0).Exp(big.NewInt(2), bits, nil)
	if !ok {
		panic("Failed interpreting RSA_KEYSIZE: " + bitsS)
	}

	for i := 0; i < count; i++ {
		// check if there is a valid secret key
		scannerSk := bufio.NewScanner(secretKeysFile)
		for scannerSk.Scan() {
			line := scannerSk.Text()
			splits := strings.Split(line, "=")
			if len(splits) != 2 {
				continue
			}
			keyname := splits[0]
			val := splits[1]
			if keyname == skPrefix+fmt.Sprint(i) {
				n, ok := big.NewInt(0).SetString(val, 10)
				if !ok {
					return false, "Failed interpreting key " + skPrefix + fmt.Sprint(i)
				}
				if n.Cmp(modulus) > 0 { // n > modulus, this is not allowed.
					fmt.Println(modulus.String())
					fmt.Println(n.String())

					return false, "Key " + skPrefix + fmt.Sprint(i) + " is greater than the modulus"
				}
				if n.Cmp(big.NewInt(0)) < 0 {
					return false, "Key " + skPrefix + fmt.Sprint(i) + " is less than 0"
				}
				break
			}
		}
	}

	// check if there is a valid entity
	for i := 0; i < count; i++ {
		entity, ok := network.FindEntityById(prefix + fmt.Sprint(i))
		if !ok {

			return false, "Could not find entity " + prefix + fmt.Sprint(i) + ". Only found up to " + prefix + fmt.Sprint(i-1) + "."
		}
		if !(entity.Type == entityType) {
			return false, "Invalid type(" + fmt.Sprint(entity.Type) + ") for " + prefix + fmt.Sprint(i)
		}
		if entity.Pk.N.Cmp(modulus) > 0 {
			return false, prefix + fmt.Sprint(i) + " has N higher than modulus"
		}
		if entity.Pk.E.Cmp(big.NewInt(3)) != 0 {
			return false, prefix + fmt.Sprint(i) + " should have E=3"
		}
	}
	return true, ""
}

/*
	Create 'count' entities with the given type and prefix.
*/
func ConstructEntities(count int, entityType int, idPrefix string) ([]network.Entity, []string) {
	entities := []network.Entity{}
	secretKeys := []*big.Int{}
	keySize := helpers.ReadInt("RSA_KEYSIZE", 3072)
	for i := 0; i < count; i++ {
		pk, sk := rsa.KeyGen(keySize)
		id := idPrefix + "_" + fmt.Sprint(i)

		address, _ := os.LookupEnv(strings.ToUpper(id) + "_ADDRESS") // if address not defined in env, just leave it empty
		entities = append(entities, network.CreateEntity(id, address, entityType, pk))
		secretKeys = append(secretKeys, sk.D)
	}
	formattedKeys := formatSecretKeys(secretKeys, idPrefix)

	return entities, formattedKeys
}

/*
	Deletes secret_keys.env and entities.json
*/
func clearKeyFiles() {
	proj_folder, ok := os.LookupEnv("proj_folder")
	if ok != true {
		panic("Could not read proj_folder env variable")
	}

	os.Remove(proj_folder + "/configuration/secret_keys.env")
	os.Remove(proj_folder + "/configuration/entities.json")
}

/*
	Helper for writing the given entity array to file.
*/
func writeEntityFile(entities []network.Entity) {
	proj_folder, ok := os.LookupEnv("proj_folder")
	if !ok {
		panic("Could not find project folder!")
	}
	entityMarshal, err := json.Marshal(entities)

	if err != nil {
		panic("Failed to marshal entities")
	}

	file, err := os.Create(proj_folder + "/configuration/entities.json")

	if err != nil {
		fmt.Println("Could not load entity file")
	}

	writer := bufio.NewWriter(file)
	for _, b := range entityMarshal {
		err := writer.WriteByte(b)
		if err != nil {
			log.Fatalf("Got error while writing to a file. Err: %s", err.Error())
		}
	}
	writer.Flush()
	file.Close()
}

/*
	Helper for writing the given secret array to file.
*/
func writeSecretFile(secrets []string) {
	proj_folder, ok := os.LookupEnv("proj_folder")
	if !ok {
		panic("Could not find project folder!")
	}

	file, err := os.Create(proj_folder + "/configuration/secret_keys.env")

	if err != nil {
		fmt.Println("Could not load entity file")
	}

	writer := bufio.NewWriter(file)
	for _, line := range secrets {
		_, err := writer.WriteString(line)
		if err != nil {
			log.Fatalf("Got error while writing to a file. Err: %s", err.Error())
		}
	}
	writer.Flush()
	file.Close()
}

/*
	Formats an array of secret keys to an array of strings composed of userID+secret key. For example, for user 1: sk_1=845731891256...
*/
func formatSecretKeys(secretKeys []*big.Int, prefix string) []string {
	linesToWrite := []string{}
	for keyID, sk := range secretKeys {
		skString := ("sk_" + prefix + "_" + fmt.Sprint(keyID) + "=" + sk.String() + "\n")
		linesToWrite = append(linesToWrite, skString)
	}
	return linesToWrite
}

func OnelineIf(condition bool, val1 interface{}, val2 interface{}) interface{} {
	if condition {
		return val1
	} else {
		return val2
	}
}
