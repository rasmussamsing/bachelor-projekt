/*
	Package that enables convenient logging.
*/
package logging

import (
	"bachelor-projekt/rsa"
	"fmt"
	"math/big"
	"os"
	"strconv"
)

// Shortcut for colors that we use for logging.
const (
	colorReset  = "\033[0m"
	colorRed    = "\033[31m"
	colorGreen  = "\033[32m"
	colorYellow = "\033[33m"
	colorBlue   = "\033[34m"
	colorPurple = "\033[35m"
	colorCyan   = "\033[36m"
	colorWhite  = "\033[37m"
	bcolorGreen = "\033[1;32m"
)

// The loglevel defines the amount of logs that will be displayed. For a given log level, all messages with that level or above will be displayed.
var logLevel int

var callers = []string{}

/*
	Stylizes the input string.
	Always prints the output, regardless of loglevel.
*/
func CriticalMessage(caller string, output string) {
	ensureLogLevelDefined()
	fmt.Println(colorGreen, "-", colorWhite, "-", colorGreen, "-", colorWhite, "-", colorGreen, "-", colorWhite, "-", colorReset)
	fmt.Println(bcolorGreen, caller+": ", output, colorWhite, colorReset)
	fmt.Println(colorGreen, "-", colorWhite, "-", colorGreen, "-", colorWhite, "-", colorGreen, "-", colorWhite, "-", colorReset)
}

/*
	Prints the input string and the caller's name. The caller's name is colored depending on the hash-value of the string.
	Prints on loglevel 3 or higher.
*/
func Info(caller string, output string) {
	ensureLogLevelDefined()
	color := findColor(caller)
	if logLevel >= 3 {
		fmt.Println(color, caller+": ", colorWhite, output, colorReset)
	}
}

/*
	Prints the input string and the caller's name. The whole line is colored yellow
	Prints on loglevel 2 or higher.
*/
func Warning(caller string, output string) {
	ensureLogLevelDefined()
	// color := findColor(caller)
	if logLevel >= 2 {
		fmt.Println(colorYellow, caller+": ", output, colorReset)
	}
}

/*
	Prints the input string and the caller's name. The whole line is colored red
	Prints on loglevel 1 or higher.
*/
func Error(caller string, output string) {
	ensureLogLevelDefined()
	// color := findColor(caller)
	if logLevel >= 1 {
		fmt.Println(colorRed, caller+": ", output, colorReset)
	}
}

/*
	Ensures that the package wide variable 'logLevel' is defined.
	Sets the logLevel that defined in env variable DEBUG_LEVEL - defaulting to 1.
*/
func ensureLogLevelDefined() {
	if logLevel == 0 {

		logLevelS, foundVar := os.LookupEnv("DEBUG_LEVEL")
		if !foundVar {
			fmt.Println("Could not find DEBUG_LEVEL in env. Using 1")
			logLevelS = "3"
		}

		l, err := strconv.Atoi(logLevelS)
		if err != nil {
			fmt.Println("Failed interpreting DEBUG_LEVEL. Using 1")
			l = 3
		}
		logLevel = l
	}
}

/*
	Computes the hash of the caller string to produce a 'random' color.
	The purpose is to randomly distribute colors between different callers,
	ensuring that each caller always receives the same color each time they call this func.
*/
func findColor(caller string) string {
	colors := []string{colorRed, colorGreen, colorYellow, colorBlue, colorPurple, colorCyan}

	// Compute hash
	callerBytes := []byte(caller)
	hash := rsa.Hash(callerBytes)
	bigint := big.NewInt(0).SetBytes(hash)

	// Mod the hash to the size of the color array, and select the color with the corresponding index.
	bigint.Mod(bigint, big.NewInt(int64(len(colors)-1)))
	index := int(bigint.Int64())
	return colors[index%len(colors)]
}
