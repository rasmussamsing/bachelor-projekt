package network

import (
	"bachelor-projekt/rsa"
	"encoding/json"
	"io/ioutil"
	"math/big"
	"os"
)

func findEntityInFile(id string, address string, entityType int, pk rsa.PublicKey) (Entity, bool) {
	proj_folder, ok := os.LookupEnv("proj_folder")
	if !ok {
		panic("Could not load proj_folder!")
	}

	data, err := ioutil.ReadFile(proj_folder + "/configuration/entities.json")

	if err != nil {
		panic(err)
	}

	var entities []Entity
	err = json.Unmarshal(data, &entities)

	if err != nil {
		panic("Failed marshalling!")
	}
	for _, entity := range entities {
		if id != "" && entity.Id != id {
			continue
		}
		if address != "" && entity.Address != address {
			continue
		}
		if entityType != -1 && entity.Type != entityType {
			continue
		}
		if pk.N.Cmp(big.NewInt(-1)) != 0 && pk.N.Cmp(entity.Pk.N) == 0 {
			continue
		}
		return entity, true
	}
	return Entity{}, false
}

func FindEntityById(id string) (Entity, bool) {
	return findEntityInFile(id, "", -1, rsa.PublicKey{N: big.NewInt(int64(-1))})
}
