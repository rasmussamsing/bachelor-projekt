/*
	Package that allows convenient communication between different nodes in our system.
	One of the features it provides is the AuthenticatedConnection, which automatically
	signs and verifies signatures on all messages sent.
*/
package network

import (
	"bachelor-projekt/logging"
	"bachelor-projekt/packet"
	"bachelor-projekt/rsa"
	"bufio"
	"encoding/json"
	"fmt"
	"math/big"
	"net"
	"os"
	"strconv"
	"time"
)

// Different types of nodes in our system.
const (
	Voter         = iota
	Tally         = iota
	Decryptor     = iota
	TrustedDealer = iota
	BulletinBoard = iota
)

// The entity struct provides a convenient abstraction for communication.
// The idea is to load a list of precomputed entities from entities.json. To connect to a certain server,
// one can simply connect to the entity for that server. This works by taking the server id (e.g. tally_server_0), and using
// it to lookup the corresponding entity object in the file. Using this abstraction, we do not have to worry about
// individual addresses or the like, when connecting to servers.
// The struct also contains Type and Pk, which makes it convenient for the network package to check for correct signature.
// The Sk field is only defined for the server itself. That is, tally_server will have a list of all the entities in the system,
// all of which will have Sk="". The only exception being the tally_server's own entity object, which will have the Sk set. The
// purpose of this is to make signing easier.
type Entity struct {
	Id      string
	Address string
	Type    int
	Pk      rsa.PublicKey
	Sk      rsa.SecretKey
}

// Wrapper around net.Listener. Also stores the calling entity.
type AuthenticatedListener struct {
	ln net.Listener
	me Entity
}

// Struct for storing incoming results from
func CreateEntity(id string, address string, entityType int, pk rsa.PublicKey) Entity {
	return Entity{Type: entityType, Id: id, Address: address, Pk: pk}
}

// Instantiate an authenticated listener on the given entity
func Listen(me Entity) AuthenticatedListener {
	ln, err := net.Listen("tcp", me.Address)
	if err != nil {
		panic(err)
	}

	return AuthenticatedListener{ln: ln, me: me}
}

// Accepts the next incoming authenticated connection.
// Also receives an identification packet from the connector - to verify identity.
func (authln *AuthenticatedListener) Accept() (AuthenticatedConnection, error) {
	conn, err := authln.ln.Accept()
	scanner := bufio.NewScanner(conn)
	if err != nil {
		return AuthenticatedConnection{}, err
	}

	//	Identification
	authconn := AuthenticatedConnection{netConn: conn, Me: authln.me, scanner: scanner}
	authconn.receiveIdentificationPacket()
	return authconn, nil
}

// Closes the connection.
func (authconn *AuthenticatedConnection) Close() {
	authconn.netConn.Close()
}

// Wrapper around net.Conn. Automatically signs and verifies signatures on all
// packets sent over the connection.
type AuthenticatedConnection struct {
	netConn net.Conn
	scanner *bufio.Scanner
	Me      Entity
	Other   Entity
}

// Connects to the given address and returns an AuthenticatedConnection.
// Automatically send ID packet, to identify the user.
func Connect(me Entity, other Entity) AuthenticatedConnection {
	conn := DialWithRetries(other.Address)
	scanner := bufio.NewScanner(conn)
	c := AuthenticatedConnection{
		netConn: conn,
		scanner: scanner,
		Me:      me,
		Other:   other}

	c.SendPacket(me.Id, packet.Identification)
	logging.Info(c.Me.Id, "Successfully connected to "+other.Id)
	return c
}

// Sends packet over the auth conn. Automatically signs the packet and awaits ack from the server.
func (c *AuthenticatedConnection) SendPacket(v interface{}, packetType string) {
	maxAmountOfTries := 10
	resendCounter := 0
	for resendCounter < maxAmountOfTries {
		resendCounter += 1
		dataMarshal, err := json.Marshal(v)
		if err != nil {
			panic(err)
		}

		p := packet.GenericPacket{ID: fmt.Sprint(packet.GenerateID()), Type: packetType, Data: dataMarshal}
		logging.Info(c.Me.Id, "Sending packet of type "+p.Type+" to "+c.Other.Id+" with ID "+p.ID)

		p.SignPacket(c.Me.Sk, c.Me.Pk)
		packetMarshal, err := json.Marshal(p)
		if err != nil {
			panic(err)
		}
		c.netConn.Write(packetMarshal)
		c.netConn.Write([]byte{'\n'})

		packetId := p.ID

		p, ok := c.receiveAck()
		if p.Type != packet.Acknowledment || !ok {
			time.Sleep(time.Second * 1)
			continue
		}

		ackedID := string(p.Data)

		if packetId == ackedID {
			return
		}
	}
}

// Send ACK on packet with given packetID
func (c *AuthenticatedConnection) sendAck(packetID string) {
	p := packet.GenericPacket{ID: fmt.Sprint(packet.GenerateID()), Type: packet.Acknowledment, Data: []byte(packetID)}

	p.SignPacket(c.Me.Sk, c.Me.Pk)
	packetMarshal, err := json.Marshal(p)
	if err != nil {
		panic(err)
	}
	c.netConn.Write(packetMarshal)
	c.netConn.Write([]byte{'\n'})
	logging.Info(c.Me.Id, "Sending ack for msg "+packetID)
}

// Awaits ID packet over the connection. The ID packet is used to figure out which entity
// connected to us. This entity is then saved on the auth conn object
func (c *AuthenticatedConnection) receiveIdentificationPacket() {
	var p packet.GenericPacket
	for c.scanner.Scan() {

		msg := c.scanner.Text()
		json.Unmarshal([]byte(msg), &p)

		if p.Type != packet.Identification {
			logging.Error(c.Me.Id, "Expected identification packet.")
			continue
		}

		var id string
		err := json.Unmarshal(p.Data, &id)
		if err != nil {
			panic(err)
		}

		c.Other, _ = FindEntityById(id)

		if !p.ValidateSignature(c.Other.Pk) {
			logging.Error(c.Me.Id, big.NewInt(0).SetBytes(p.Signature).String())
			panic(c.Me.Id + ": Received invalid signature on identification packet.")
		}

		c.sendAck(p.ID)
		return
	}
}

// Awaits ACK on the authenticated connection. Returns the packet
func (c *AuthenticatedConnection) receiveAck() (packet.GenericPacket, bool) {
	var p packet.GenericPacket

	for c.scanner.Scan() {
		msg := c.scanner.Text()
		json.Unmarshal([]byte(msg), &p)
		if !p.ValidateSignature(c.Other.Pk) {
			logging.Error(c.Me.Id, "Received invalid signature on Ack from sender "+c.Other.Id)
			continue
		}

		return p, true
	}
	logging.Error(c.Me.Id, "Failed to get a reply from "+c.Other.Id)
	return packet.GenericPacket{}, false
}

// Awaits the next packet on the connection. Automatically verifies the signature.
func (c *AuthenticatedConnection) ReceivePacket() packet.GenericPacket {
	var p packet.GenericPacket

	for c.scanner.Scan() {
		msg := c.scanner.Text()
		json.Unmarshal([]byte(msg), &p)
		logging.Info(c.Me.Id, "Received packet from "+c.Other.Id+" with ID "+p.ID+" and type "+p.Type)
		if !p.ValidateSignature(c.Other.Pk) {
			logging.Error(c.Me.Id, "Received invalid signature from sender "+c.Other.Id)
			continue
		}
		break
	}
	if c.scanner.Err() != nil {
		panic(c.scanner.Err().Error())
	}
	c.sendAck(p.ID)
	return p
}

// Dials an address. If the dialing fails, it tries again after 200ms, up to a maximum
// amount of times corresponding to the CONNECTION_RETRIES env variable. If no such variable
// is provided, it defaults to 5 max retries. If the connection cannot be established after
// max_retries has been exceeded, it panics.
// Returns the connection if one is found.
func DialWithRetries(addr string) net.Conn {
	sleep_duration := 200 * time.Millisecond
	max_retries_str, ok := os.LookupEnv("CONNECTION_RETRIES")
	max_retries, err := strconv.Atoi(max_retries_str)
	if !ok {
		logging.Warning("ENVIRONMENT", "Could not find CONNECTION_RETRIES in env. Defaulting to 5")
		max_retries = 5
	}
	if err != nil {
		logging.Warning("ENVIRONMENT", "MAX_RETRIES could not be parsed as an int. Defaulting to 5")
		max_retries = 5
	}
	retries := 0

	for {
		conn, err := net.Dial("tcp", addr)
		if err != nil {
			time.Sleep(sleep_duration)

			if retries > max_retries {
				panic("Could not connect to address " + addr + ". Max_retries exceeded.")
			} else {
				retries += 1
				continue
			}
		}
		return conn
	}
}

// Convenient wrapper around DialWithRetries, that looks up the given env_var
// and uses it as address in the call to DialWithRetries.
// Returns the connection if one is found.
func DialAddrFromEnvWithRetries(env_var string) net.Conn {
	addr, ok := os.LookupEnv(env_var)
	if !ok {
		panic("Could not retrieve " + env_var + " in env. Exiting.")
	}
	return DialWithRetries(addr)
}

// Loads env_var from the environment, and listens on that address for new connections.
// Returns the listener.
func ListenAddrFromEnv(env_var string) net.Listener {
	addr, ok := os.LookupEnv(env_var)
	if !ok {
		panic("Could not lookup " + env_var + " in env. Exiting.")
	}

	ln, err := net.Listen("tcp", addr)
	if err != nil {
		panic(err)
	}

	return ln
}
