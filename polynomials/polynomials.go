/*
	Packet used for the polynomial functionality required in the secret sharing protocol between decryptors.
	Contains functionality to generate a random polynomial of a given degree, split it
	into shares, reconstruct it, and find the intersection of the polynomial with the y-axis.
*/
package polynomials

import (
	"bachelor-projekt/elgamal"
	"bachelor-projekt/helpers"
	"bachelor-projekt/logging"
	"fmt"
	"math/big"
)

// Takes a polynomial as input, and finds the corresponding y-values of the x's=[1, 2, 3...] up to 'count'.
// Return the list of y-values generated.
func GenerateShares(p Polynomial, count int) []*big.Int {
	shares := []*big.Int{}
	for x := 1; x <= count; x++ {
		value := p.ComputeAt(big.NewInt(int64(x)))
		shares = append(shares, value)
	}
	return shares
}

// Computes the y-val of the given polynomial at the given x-value.
func (p *Polynomial) ComputeAt(x *big.Int) *big.Int {
	val := big.NewInt(0)
	for i := 0; i < p.degree+1; i++ {
		val.Add(val, big.NewInt(0).Mul(p.terms[i], big.NewInt(0).Exp(x, big.NewInt(int64(i)), p.mod)))
	}
	return val.Mod(val, p.mod)
}

// Generates a random Polynomial of the given degree, with constant set to the given one.
func GenerateRandomPolynomial(constant *big.Int, degree int, pk elgamal.Public_key) Polynomial {
	terms := []*big.Int{constant}
	for i := 0; i < degree; i++ {
		a_i := big.NewInt(0).Mod(helpers.RandomNum(1024), &pk.Q)

		terms = append(terms, a_i)
	}
	p := Polynomial{terms: terms, degree: degree, mod: &pk.Q}
	logging.Info("Polynomials", p.String())
	return p
}

// Helper for converting polynomial to a string.
func (p *Polynomial) String() string {
	s := "y = " + p.terms[0].String() + " + "
	for i := 0; i < p.degree; i++ {
		term := p.terms[i+1].String() + "x" + "^" + fmt.Sprint(i+1) + " + "
		s = s + term
	}
	return s[0 : len(s)-2]
}

// Given a set of points in the exponent, computes the intersection of those points with the y-axis.
func IntersectionFromPoints(points []Point, mod *big.Int, degree int) *big.Int {
	result := big.NewInt(1)
	lambdas := []*big.Int{}
	if len(points) < degree {
		logging.Error("polynomials", "Not enough points!")
	}
	points = points[0 : degree+1]

	for _, point := range points {
		product := big.NewInt(1)
		for _, other := range points {
			if other.X1.Cmp(point.X1) == 0 {
				continue
			}

			divisor := big.NewInt(0).Sub(other.X1, point.X1)
			divisor = divisor.Mod(divisor, mod)
			invDivisor := helpers.ModularInverse(divisor, mod)
			val := big.NewInt(0).Mul(other.X1, invDivisor)
			product.Mul(product, val)
			product.Mod(product, mod)
		}
		lambdas = append(lambdas, product)
	}
	for i, point := range points {
		highmod := big.NewInt(0).Add(big.NewInt(1), big.NewInt(0).Mul(mod, big.NewInt(2)))
		val := big.NewInt(0).Exp(point.X2, lambdas[i], highmod)

		result.Mul(result, val)
		result.Mod(result, highmod)
	}
	logging.Info("polynomials", "Result: "+result.String())
	return result
}

// Struct for representing a polynomial
type Polynomial struct {
	degree int
	terms  []*big.Int
	mod    *big.Int
}

// Represents a point on the polynomial
type Point struct {
	X1 *big.Int
	X2 *big.Int
}
