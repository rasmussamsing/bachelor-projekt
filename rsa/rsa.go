package rsa

import (
	"crypto/rand"
	"crypto/sha256"
	"math/big"
)

// Decrypts the given message under the given private key
func Encrypt(message *big.Int, publicKey PublicKey) *big.Int {
	ciphertext := big.NewInt(0).Exp(message, publicKey.E, publicKey.N)
	return ciphertext
}

// Encrypt the given message under the given public key
func Decrypt(ciphertext *big.Int, secretKey SecretKey) *big.Int {
	message := big.NewInt(0).Exp(ciphertext, secretKey.D, secretKey.N)
	return message
}

// Computes hash of given byte array
func Hash(m []byte) []byte {
	h := sha256.New()
	h.Write(m)
	return h.Sum(nil)
}

// Takes n and d as input and construct corresponding Pk and Sk, assuming E=3
func ConstructKeys(n *big.Int, d *big.Int) (PublicKey, SecretKey) {
	pk := PublicKey{N: n, E: big.NewInt(3)}
	sk := SecretKey{D: d, N: n}
	return pk, sk
}

// Generates Pk and Sk with 'k' bitlength
func KeyGen(keySize int) (PublicKey, SecretKey) {
	e := big.NewInt(3)
	// generate prime1 and prime22
	var prime1 = CreatePrime(keySize)
	var prime2 = CreatePrime(keySize)
	var primeProduct = big.NewInt(0).Mul(prime1, prime2)

	// calculate private exponent d
	d := big.NewInt(0)
	prime1subOne := big.NewInt(0).Sub(prime1, big.NewInt(1))
	prime2subOne := big.NewInt(0).Sub(prime2, big.NewInt(1))
	primesSubedMult := big.NewInt(0).Mul(prime1subOne, prime2subOne)
	d = big.NewInt(0).ModInverse(e, primesSubedMult)

	//Create public and secret-key struct
	pk := PublicKey{primeProduct, e}
	sk := SecretKey{primeProduct, d}

	return pk, sk
}

// Generates a prime of the given order
func CreatePrime(k int) *big.Int {
	e := big.NewInt(3)
	var prime *big.Int
	divisor := big.NewInt(0)
	GCDisNotOne := 1
	for GCDisNotOne == 1 {
		prime, _ = rand.Prime(rand.Reader, k/2)
		GCDisNotOne = divisor.GCD(nil, nil, e, big.NewInt(0).Sub(big.NewInt(1), prime)).Cmp(big.NewInt(1))
	}
	return prime
}

type PublicKey struct {
	N *big.Int
	E *big.Int
}

type SecretKey struct {
	N *big.Int
	D *big.Int
}
