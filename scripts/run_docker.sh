# Scripts that creates a Docker image, creates a container, and then runs the 
# container. Our project is then run inside the container, to provide an identical 
# environment across different hardware.
PROJECT_FOLDER=$(pwd)
cont_name=bachelor-projekt
hasimage="$(docker image ls | grep bachelor-image)"
testconst=""
output=hackySolution.txt
echo > $output
if [ -z "$hasimage" ]
then
  docker build --tag bachelor-image .
fi

while getopts 'bt' opts
do
  case $opts in
    b) container_id=$(docker ps -aqf "name=$cont_name")
       docker build --tag bachelor-image .
       if [ -n $container_id ]
       then
         echo removing container $container_id
         echo $(docker container rm $container_id) > /dev/null
       fi
       echo $(docker create -p 8080:8080 -v $PROJECT_FOLDER:/go/src/bachelor-projekt --name $cont_name -it bachelor-image) > /dev/null
       echo Created new container with name $cont_name
       hascontainer="true"
       ;;
    t)
      echo runtest > $output
      ;;
  esac
done

hascontainer=$(docker ps -aqf "name=$cont_name") $hascontainer

if [ -z $hascontainer ]
then
  docker create --net=host -v $PROJECT_FOLDER:/usr/local/go/bachelor --name $cont_name -it bachelor-image ; echo Created new container with name $cont_name
fi

echo ----- RUNNING PROGRAM -----
docker start -i $cont_name
echo " "
echo ----- END OF PROGRAM -----
