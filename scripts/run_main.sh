export proj_folder=/go/src/bachelor-projekt
export $(grep -v '^#' $proj_folder/configuration/bach.env | xargs)

go env -w GO111MODULE=off

cd $proj_folder/elgamal
go build
echo Built elgamal

cd $proj_folder/packet
go build 
echo Built packet 

cd $proj_folder/tallying_server
go build .
go install .
echo Built tallying_server

cd $proj_folder/helpers
go build .
go install .
echo Built helpers

cd $proj_folder/decrypting_server
go build .
go install .
echo Built decrypting_server

cd $proj_folder/trusted_dealer
go build .
go install .
echo Built trusted_dealer

cd $proj_folder/logging
go build .
go install .
echo Built log

cd $proj_folder/polynomials
go build .
go install .
echo Built polynomials

cd $proj_folder/bulletin_board
go build .
go install .
echo Built bulletin_board

cd $proj_folder/key_generator
go build .
go install .
echo Built key_generator

echo Running key_generator
go run bachelor-projekt/key_generator
if [[ $? == 1 ]]; then
    echo Failed key_generation
    exit 1
fi
export $(grep -v '^#' $proj_folder/configuration/bach.env | xargs)

cd ..
if [[ $(head -1 hackySolution.txt) = "runtest" ]]; then
    echo Running Testcases
    go clean -testcache && go test ./...
    echo Done Testing
fi


export $(grep -v '^#' $proj_folder/configuration/secret_keys.env | xargs)

go run bachelor-projekt/bulletin_board $sk_bulletin_board_0 bulletin_board_0 &
go run bachelor-projekt/tallying_server $sk_tally_0 tally_0 &

for (( i=0; i<$DECRYPT_COUNT; i++ ))
do
sk=sk_decryptor_$i
go run bachelor-projekt/decrypting_server ${!sk} decryptor_$i &
done

go run bachelor-projekt/trusted_dealer $sk_trusted_dealer_0 trusted_dealer_0 &

sleep 1s

while getopts 'd:' opts
do
    case $opts in 
     d) debug_file=$proj_folder'/'$OPTARG'/'$OPTARG
        echo $debug_file
        echo $OPTARG
        dlv attach --headless --listen=:2345 $(pgrep $OPTARG) $debug_file &
    esac
done

# Start clients

cd client
go build .
for (( i=0; i<$VOTER_COUNT; i++ ))
do
sk=sk_voter_$i
go run bachelor-projekt/client ${!sk} voter_$i &
done

# Wait for all jobs to finish

for job in `jobs -p`
do
    wait $job || let "FAIL+=1"
done