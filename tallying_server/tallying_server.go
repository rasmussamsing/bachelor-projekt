// Code for the tallying server. The tallying server accepts encrypted votes
// from voters. Once the server has been running for 'SERVER_UP_TIME' (defined in bach.env)
// seconds, it rejects new connections and sends the results to the decrypting server.

package main

import (
	"bachelor-projekt/elgamal"
	"bachelor-projekt/helpers"
	"bachelor-projekt/logging"
	"bachelor-projekt/network"
	"bachelor-projekt/packet"
	zkproofs "bachelor-projekt/zk_proofs"
	"encoding/json"
	"fmt"
	"math/big"
	"os"
	"strconv"
	"sync"
	"time"
)

// Used to close down the server. Calls itself after one second.
func delayedCall(me network.Entity) {
	time.Sleep(5 * time.Second)
	_ = network.Connect(me, me)
}

/*
	Listen for votes for SERVER_UP_TIME seconds. Each valid vote is then summed up, and the
	result is sent to decrypting server
*/
func main() {
	// Interrupt channel is used to send interrupts to the listener goroutine.
	interruptChannel := make(chan string)

	// Reply channel is used for to wait for the other goroutines to finish
	// before main shuts down
	replyChannel := make(chan string)

	// Retrieve pk and sk from files
	_, sk := helpers.GetKeyPair()
	me, _ := network.FindEntityById(os.Args[2])
	me.Sk = sk
	logging.Info(me.Id, "Booted!")

	// Setup packet receiver:
	go listen(interruptChannel, replyChannel, me)
	start := time.Now()

	// Once enough time has elapsed, shut down the server
	for {
		timeElapsed, _ := strconv.Atoi(os.Getenv("SERVER_UP_TIME"))
		if time.Since(start) > time.Millisecond*1000*time.Duration(timeElapsed) {
			logging.Info(me.Id, "Stopping server.")

			go delayedCall(me)
			interruptChannel <- "sendResults"
			break
		}
	}

	<-replyChannel // wait for listener to finish closing all connections
	// shutDownTrustedDealer(me)
	logging.CriticalMessage(me.Id, "Shutting down")
}

/*
	Helper for fetchen Elgamal PK from trusted dealer
*/
func getPublicKey(state *ServerState, me network.Entity) elgamal.Public_key {
	// Send PK request
	dealer, _ := network.FindEntityById("trusted_dealer_0")
	conn := network.Connect(me, dealer)
	defer conn.Close()

	conn.SendPacket([]byte{}, packet.PkRequest)
	p := conn.ReceivePacket()

	// Retrieve PK
	var pk elgamal.Public_key

	if p.Type == packet.TDPk {
		json.Unmarshal(p.Data, &pk)
	} else {
		panic("Received wrong packet type")
	}
	return pk
}

/*
	Goroutine for accepting new connections from voters.
	interruptChannel: Used for sending interrupts such as the interruption to send results to decrypt servers.
	me: The tally server entity, used for establishing new connections
*/
func listen(interruptChannel chan string, replyChannel chan string, me network.Entity) {
	// Instantiate the ServerState
	state := ServerState{voteChannel: make(chan elgamal.Ciphertext), CurrC1: big.NewInt(1), CurrC2: big.NewInt(1), connections: []network.AuthenticatedConnection{}, connectionsLock: &sync.Mutex{}, voterLock: &sync.Mutex{}, alreadyVoted: []string{}}

	// Fetch Elgamal PK
	pk := getPublicKey(&state, me)
	state.Pk = pk

	tallyInterrupt := make(chan string)

	// Start the tally listener
	go tally(state, tallyInterrupt)

	// Listen for connections
	authln := network.Listen(me)

	for {
		// When one is found, hand the connection over to handleConnection
		conn, err := authln.Accept()
		if err != nil {
			panic("Could not accept incoming connection. Exiting.")
		}
		select {
		case interrupt := <-interruptChannel:
			if interrupt == "sendResults" { // Send results to decryptor, then handle connection
				logging.Info(me.Id, "Got the send results interruption.")
				go sendResultsToDecrypt(me, state)
				go handleConnection(conn, &state)
				state.connectionsLock.Lock()
				state.connections = append(state.connections, conn)
				state.connectionsLock.Unlock()
			}
		default:
			go handleConnection(conn, &state)
			state.connectionsLock.Lock()
			state.connections = append(state.connections, conn)
			state.connectionsLock.Unlock()
		}
	}
}

/*
	Connection handler that receives the encrypted vote from voters.
   	If the packet type is anything else than packet.Vote, it is simply
 	ignored.
	authConn: connection to handle
	state: current serverState
*/
func handleConnection(authConn network.AuthenticatedConnection, state *ServerState) {
	p := authConn.ReceivePacket()
	if p.Type == packet.Vote && authConn.Other.Type == network.Voter {
		state.voterLock.Lock()
		defer state.voterLock.Unlock()
		// First we check if this user has voted before
		if voterInArr(authConn.Other.Pk.N.String(), state.alreadyVoted) {
			logging.Warning(authConn.Me.Id, authConn.Other.Id+" already voted.")
			return
		}

		// Verify the OR proof passed with the vote
		var cipherProof zkproofs.CipherAndProof
		e := json.Unmarshal(p.Data, &cipherProof)

		if e != nil {
			panic(e)
		}

		verifySuccess := zkproofs.VerifyORProof(cipherProof.Proof, cipherProof.Cipher, state.Pk, authConn.Other.Id)

		if !verifySuccess {
			logging.Warning(authConn.Me.Id, "Received invalid proof from "+authConn.Other.Id)
			return
		}

		// If successful, send the cipher to the tally listener over voteChannel, and append user
		// to alreadyVoted array.
		state.alreadyVoted = append(state.alreadyVoted, authConn.Other.Pk.N.String())
		state.voteChannel <- cipherProof.Cipher
		return
	}
}

// Sends the encrypted voteSum to the decrypting server.
func sendResultsToDecrypt(me network.Entity, state ServerState) {
	cipher := elgamal.Ciphertext{C1: state.CurrC1, C2: state.CurrC2}
	decryptorCount := helpers.ReadInt("DECRYPT_COUNT", 1)
	for i := 0; i < decryptorCount; i++ {
		decryptor, _ := network.FindEntityById("decryptor_" + fmt.Sprint(i))
		authConn := network.Connect(me, decryptor)
		authConn.SendPacket(&cipher, packet.VoteSum)
	}
}

/*
	Goroutine for receiving verified votes from handleConnection. Each new vote is
	Multiplied onto the current product.
*/
func tally(state ServerState, interruptChannel chan string) {
	for true {
		var ciphers elgamal.Ciphertext
		select {
		// If an interrupt is detected, we shut down the goroutine.
		case interrupt := <-interruptChannel:
			if interrupt == "stop" {
				return
			}
		case ciphers = <-state.voteChannel:
		}
		state.CurrC1.Mul(state.CurrC1, ciphers.C1)
		state.CurrC2.Mul(state.CurrC2, ciphers.C2)
		state.CurrC1.Mod(state.CurrC1, &state.Pk.P)
		state.CurrC2.Mod(state.CurrC2, &state.Pk.P)
	}
}

/*
	Checks if the given user has already voted.
*/
func voterInArr(user string, arr []string) bool {
	for _, currUser := range arr {
		if user == currUser {
			return true
		}
	}
	return false
}

// Struct for conveniently representing the current state of the tallying server.
type ServerState struct {
	Pk              elgamal.Public_key
	CurrC1          *big.Int
	CurrC2          *big.Int
	voteChannel     chan (elgamal.Ciphertext)
	connections     []network.AuthenticatedConnection
	connectionsLock *sync.Mutex
	alreadyVoted    []string
	voterLock       *sync.Mutex
}
