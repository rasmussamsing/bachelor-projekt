/*
	The trusted dealer is responsible for generating the elgamal pk and sk, and for generating all the secrets that
	are sent to the decrypting servers. It is also responsible for distributing h_i values.
*/
package main

import (
	"bachelor-projekt/elgamal"
	"bachelor-projekt/helpers"
	"bachelor-projekt/logging"
	"bachelor-projekt/network"
	"bachelor-projekt/packet"
	"bachelor-projekt/polynomials"
	"bachelor-projekt/rsa"
	"encoding/json"
	"math/big"
	"os"
	"strconv"
)

var shares = []*big.Int{}

/*
	Setup server.
*/
func main() {
	// t   : # of people required to get secret
	// n   : # of shares
	// t-1 : degree of polynomial
	_, sk_rsa := helpers.GetKeyPair()
	me, _ := network.FindEntityById(os.Args[2])
	me.Sk = sk_rsa
	logging.Info(me.Id, "Booted!")

	tS, ok := os.LookupEnv("THRESHOLD")
	if !ok {
		logging.Warning("Trusted Dealer", "Could not find THRESHOLD in env. Defaulting to 1")
		tS = "1"
	}
	t, err := strconv.Atoi(tS)
	if err != nil {
		logging.Warning("Trusted Dealer", "Failed to interpret THRESHOLD. Defaulting to 1")
		t = 1
	}

	nS, ok := os.LookupEnv("DECRYPT_COUNT")
	if !ok {
		logging.Warning("Trusted Dealer", "Could not find DECRYPT_COUNT in env. Defaulting to 1")
		nS = "1"
	}
	n, err := strconv.Atoi(nS)
	if err != nil {
		logging.Warning("Trusted Dealer", "Failed to interpret DECRYPT_COUNT. Defaulting to 1")
		n = 1
	}

	// generate elgamal pk and sk. Use these to generate a random polynomial. We then use the polynomial
	// to derive shares for all decryptors.
	p, q := helpers.EnsurePrimesInEnv()
	pk, sk := elgamal.Generate_keys(p, q)
	polynomial := polynomials.GenerateRandomPolynomial(&sk.X, t-1, pk)

	shares = polynomials.GenerateShares(polynomial, n)

	// setup listener
	ln := network.Listen(me)

	for {
		conn, err := ln.Accept()
		if err != nil {
			// panic
			panic(err)
		}
		shouldQuit := handleConnection(conn, shares, pk, sk)
		if shouldQuit {
			break
		}
	}
	logging.CriticalMessage(me.Id, "Shutting down")
}

/*
	Handles connection
*/
func handleConnection(conn network.AuthenticatedConnection, shares []*big.Int, pk elgamal.Public_key, sk elgamal.Secret_key) bool {
	p := conn.ReceivePacket()
	// Send secret share to decryptor that requests it
	if p.Type == packet.ShareRequest && conn.Other.Type == network.Decryptor {
		decryptor_id, err := strconv.Atoi(conn.Other.Id[10:])
		if err != nil {
			panic("Could not parse decryptor_id")
		}
		share := shares[decryptor_id]
		encryptedShare := rsa.Encrypt(share, conn.Other.Pk)
		conn.SendPacket(&encryptedShare, packet.SecretShare)
		return false
	}

	// Send Elgamal PK to the requestor. Anyone can request the PK
	if p.Type == packet.PkRequest {
		conn.SendPacket(&pk, packet.TDPk)
		return false
	}

	if p.Type == packet.ShutDown && conn.Other.Type == network.Tally {
		return true
	}

	// Reply with the H_i val of the requested decryptor. H_i = g^s_i
	if p.Type == packet.HRequest {
		var decryptID string
		err := json.Unmarshal(p.Data, &decryptID)
		if err != nil {
			panic(err)
		}
		decryptor_id, err := strconv.Atoi(decryptID[10:])
		if err != nil {
			panic("Could not parse decryptor_id")
		}
		// Get the share val, and raise G to the this power.
		share := shares[decryptor_id]
		h := big.NewInt(0).Exp(&pk.G, share, &pk.P)
		logging.Info(conn.Me.Id, "Returning "+h.String())
		conn.SendPacket(&h, packet.H)
	}

	return false
}
